
import java.util.ArrayList;

/**
 *
 * @author De
 */
public class VehicleImages {
    private  String vin;
    private  ArrayList<String> images = new ArrayList<>();

    public VehicleImages() {
    }

    public VehicleImages(String vin, ArrayList<String> array) {
        this.vin = vin;
        this.images = array;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
    
    
    
        
       
    
}
