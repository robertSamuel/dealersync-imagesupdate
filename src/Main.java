
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;

/**
 *
 * @author De
 */
public class Main {

    private static String archCSV;
    private static String historyCSV;
    private static String serverName;
    private static int serverPort;
    private static String serverDB;
    private static String serverUserName;
    private static String serverPassword;
    private static Connection conn;
    private static String fileLocation;
    private static Statement st;
    private static ArrayList companies;
    private static String dealerId;
    private static String keyPassPhrase;
    private static String keyUrl;
    private static ArrayList<String> imagesId;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClientProtocolException, IOException, FileNotFoundException {
        CSVReader csvReader;
        String[] row = null;
        ArrayList<String> array = new ArrayList<>();
        imagesId = new ArrayList<>();

        try {
            SQLServerDataSource dSource = new SQLServerDataSource();
            dSource.setApplicationName("Dealer Sync Update Images App");

            File connectionFile = new File("Connection.dll");
            fileLocation = connectionFile.getAbsolutePath();
            fileLocation = fileLocation.replace("Connection.dll", "");

            if (connectionFile.exists()) {
                BufferedReader bufferedReader;

                bufferedReader = new BufferedReader(new FileReader(connectionFile));

                serverName = bufferedReader.readLine();
                serverPort = Integer.parseInt(bufferedReader.readLine());
                serverUserName = bufferedReader.readLine();
                serverPassword = bufferedReader.readLine();
                bufferedReader.close();

                dSource.setServerName(serverName);
                dSource.setPortNumber(serverPort);
                dSource.setUser(serverUserName);
                dSource.setPassword(serverPassword);
            }

            File propertiesFile = new File(fileLocation + "DealerSyncImages\\DealerSyncImagesProperties.dll");

            if (propertiesFile.exists()) {
                companies = new ArrayList();

                BufferedReader bufferedReader = new BufferedReader(new FileReader(propertiesFile));
                String strLine;

                while ((strLine = bufferedReader.readLine()) != null) {
                    companies.add(strLine);
                }
                bufferedReader.close();
            }

            for (int i = 0; i < companies.size(); i++) {
                StringTokenizer tokens = new StringTokenizer((String) companies.get(i), ":");
                serverDB = tokens.nextToken();
                dealerId = tokens.nextToken();
                keyUrl = tokens.nextToken();
                keyPassPhrase = tokens.nextToken();
                archCSV = tokens.nextToken();
                historyCSV = tokens.nextToken();
                dSource.setDatabaseName(serverDB);

                conn = dSource.getConnection();
                st = conn.createStatement();

                csvReader = new CSVReader(new FileReader(archCSV));

                while ((row = csvReader.readNext()) != null) {
                    array.add(row[2]);
                }

                array.remove(0);
                csvReader.close();
                array.clear();

                array.add("1ZVBP8CH2A5101658");
                array.add("KNDJJ741485009280");
                array.add("3N1CN7AP7CL861642");

                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                keyStore.load(new FileInputStream(fileLocation + "DealerSyncImages\\" + keyUrl), keyPassPhrase.toCharArray());
                SSLContext sslContext = SSLContexts.custom().loadKeyMaterial(keyStore, keyPassPhrase.toCharArray()).build();
                HttpClient httpClient = HttpClientBuilder.create().setSSLContext(sslContext).build();//HttpClients.custom().setSSLContext(sslContext).build();

                //requestGet(array, httpClient, imagesId);
                //requestDelete(array, httpClient);
                requestAdd(array, httpClient);
            }

//               URL url = new URL("https://api-test.dealersync.com/v1/Vehicle/Images/Get?DealerId=99&Vin=3N1CN7AP7CL861642");//your url i.e fetch data from .
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("GET");
//            conn.setRequestProperty("Accept", "application/json");
//            if (conn.getResponseCode() != 200) {
//                throw new RuntimeException("Failed : HTTP Error code : "
//                        + conn.getResponseCode());
//            }
//            InputStreamReader in = new InputStreamReader(conn.getInputStream());
//            BufferedReader br = new BufferedReader(in);
//            String output;
//            while ((output = br.readLine()) != null) {
//                System.out.println(output);
//            }
//            conn.disconnect();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void requestGet(ArrayList<String> arrayToGet, HttpClient httpClient, ArrayList<String> vehicleImageId) {
        HttpUriRequest httpUriRequest;
        HttpResponse response;
        ArrayList<VehicleImages> vehicleImages = new ArrayList<>();
        VehicleImages v;
        String json = "";
        int n = 0;

        try {
            for (int i = 0; i < arrayToGet.size(); i++) {
                httpUriRequest = new HttpGet("https://api-test.dealersync.com/v1/Vehicle/Images/Get?DealerId=" + dealerId + "&Vin=" + arrayToGet.get(i));
                System.out.println("Loop: " + i);
                response = httpClient.execute(httpUriRequest);
                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";

                while ((line = br.readLine()) != null) {
                    json += line;
                    System.out.println(n);
                    System.out.println(line);
                    n++;
                }

                //VehicleImages vii = new Gson().fromJson(json, VehicleImages.class);
                //System.out.println(vii.getImages());
            }
            System.out.println("starts\n" + json);
            int start = json.indexOf("[", 0);
            int end = json.indexOf("]", 0);

            System.out.println("\nData:\n" + json.substring(start + 1, end));
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void requestDelete(ArrayList<String> arrayToDelete, HttpClient httpClient) {
        HttpUriRequest httpUriRequest;
        HttpResponse response;

        try {
            for (int i = 0; i < arrayToDelete.size(); i++) {
                httpUriRequest = new HttpPost("https://api-test.dealersync.com/v1/Vehicle/Images/Delete?DealerId=" + dealerId + "&Vin=" + arrayToDelete.get(i));
                System.out.println(httpUriRequest.toString());
                response = httpClient.execute(httpUriRequest);
                BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void requestAdd(ArrayList<String> addList, HttpClient httpClient) throws SQLException, IOException {
        HttpUriRequest httpUriRequest;
        HttpResponse response;
        String vins = "";

        for (int i = 1; i <= addList.size() - 1; i++) {
            vins += "or VIN = '" + addList.get(i) + "'\n";
        }

        String query = "SELECT VIN, Image0,\n"
                + "	Image1,\n"
                + "	Image2,\n"
                + "	Image3,\n"
                + "	Image4,\n"
                + "	Image5,\n"
                + "	Image6,\n"
                + "	Image7,\n"
                + "	Image8,\n"
                + "	Image9,\n"
                + "	Image10,\n"
                + "	Image11,\n"
                + "	Image12,\n"
                + "	Image13,\n"
                + "	Image14,\n"
                + "	Image15,\n"
                + "	Image16,\n"
                + "	Image17,\n"
                + "	Image18,\n"
                + "	Image19,\n"
                + "	Image20,\n"
                + "	Image21,\n"
                + "	Image22,\n"
                + "	Image23,\n"
                + "	Image24,\n"
                + "	Image25,\n"
                + "	Image26,\n"
                + "	Image27,\n"
                + "	Image28,\n"
                + "	Image29,\n"
                + "	Image30,\n"
                + "	Image31,\n"
                + "	Image32,\n"
                + "	Image33,\n"
                + "	Image34,\n"
                + "	Image35,\n"
                + "	Image36,\n"
                + "	Image37,\n"
                + "	Image38,\n"
                + "	Image39,\n"
                + "	Image40,\n"
                + "	Image41,\n"
                + "	Image42,\n"
                + "	Image43,\n"
                + "	Image44,\n"
                + "	Image45,\n"
                + "	Image46,\n"
                + "	Image47,\n"
                + "	Image48,\n"
                + "	Image49,\n"
                + "	Image50,\n"
                + "	Image51,\n"
                + "	Image52,\n"
                + "	Image53,\n"
                + "	Image54,\n"
                + "	Image55,\n"
                + "	Image56,\n"
                + "	Image57,\n"
                + "	Image58,\n"
                + "	Image59\n"
                + "FROM InventoryTable\n"
                + " WHERE VIN = '1FMEU73847UB54424'"
                + " OR VIN = '5GAEV23D79J155297'"
                + " OR VIN = '1GKDS13S982146562'"
                //                + "" + vins + ""
                + "ORDER BY StockNumber DESC";

        ResultSet rs = st.executeQuery(query);

        ArrayList<VehicleImages> vehicleImages = new ArrayList<>();
        ArrayList<String> image;
        VehicleImages v;
        HttpPost htp;
        int n = 0;

        ResultSetMetaData rsmd = rs.getMetaData();
        int colNo = rsmd.getColumnCount();

        BufferedWriter bw = new BufferedWriter(new FileWriter(fileLocation + "DealerSyncImages\\" + historyCSV));
        String s = "";
        for (int i = 0; i < colNo; i++) {
            if (i == 0) {
                s = "\"" + rsmd.getColumnName(i + 1) + "\"";
            } else {
                s = s + ",\"" + rsmd.getColumnName(i + 1) + "\"";
            }
        }
        bw.write(s);
        bw.newLine();

        //System.out.println("starting to add images of each vehicle");
        while (rs.next()) {

            String[] values = new String[colNo];
            image = new ArrayList<>();
            //System.out.println(colNo);
            //System.out.println(rs.getString("VIN"));

            for (int i = 0; i < colNo; i++) {
                String d = rs.getString(i + 1);
                    if (i != 0 && (rs.getString(i + 1) != null)) {
                        image.add(rs.getString(i + 1));
                    }

            }

            vehicleImages.add(new VehicleImages(addList.get(n), image));
            v = vehicleImages.get(n);

            String data = "";

            for (int j = 0; j < v.getImages().size(); j++) {
                data += "\n\t\"" + v.getImages().get(j) + "\",";
            }

            String finalData = "[" + data.substring(0, data.length() - 1) + "\n]";
            //System.out.println(finalData);

            htp = new HttpPost("https://api-test.dealersync.com/v1/Vehicle/Images/Add?DealerId=" + dealerId + "&Vin=" + v.getVin());
            StringEntity entity = new StringEntity(finalData, "UTF-8");
            entity.setContentType("application/json");
            htp.setEntity(entity);
            httpUriRequest = htp;

            response = httpClient.execute(httpUriRequest);
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            
            while ((line = br.readLine()) != null) {
                System.out.println(line + "***");
                ArrayList<String> arrayToken = new ArrayList<>();
                String word = "VehicleImageId\":";
                String [] extracted = line.split(word);
                
                for (int i = 1; i < extracted.length; i++) {
                    String[] delimeter = extracted[i].split(",\"ImageUrl"); //substring(0, extracted[i].indexOf(",ImageUrl"));
                    values[i-1] = delimeter[0];
                    //System.out.println(extracted[i] + "*");
                    System.out.println(delimeter[0]);
                }
                    System.out.println(extracted.length);
            }
            
            String g = "";
            for (int z = 0; z < values.length; z++) {
                //System.out.println(values[z]);
                if (values[z].startsWith(",")) {
                    g = g + "\"" + values[z].substring(1) + "\",";
                } else {
                    g = g + "\"" + values[z] + "\",";
                }

            }
            g = g.substring(0, g.length() - 1);
            g = g.replace("\n", "");
            bw.write(g);
            bw.newLine();

            n++;
        }

        bw.close();
        rs.close();
        st.close();
        conn.close();

    }
}
